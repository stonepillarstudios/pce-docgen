<?php
/**
 * Created by PhpStorm.
 * User: george
 * Date: 2018/07/05
 * Time: 10:52 AM
 */


$to_send = json_decode(file_get_contents('./example-corp.json'));

$curl = curl_init();
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($to_send));

curl_setopt($curl, CURLOPT_URL, 'https://docgen.stonepillarstudios.com/v1/generator');

curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$result = curl_exec($curl);

curl_close($curl);

echo $result;

?>